package cn.tedu.mapper;

import cn.tedu.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author liu
 */
public interface UserMapper extends BaseMapper<User> {

}
