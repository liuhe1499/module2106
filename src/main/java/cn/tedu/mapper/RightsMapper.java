package cn.tedu.mapper;

import cn.tedu.pojo.Rights;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author liu
 */
public interface RightsMapper extends BaseMapper<Rights> {
}
