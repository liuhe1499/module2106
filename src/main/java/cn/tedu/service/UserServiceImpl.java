package cn.tedu.service;

import cn.tedu.mapper.UserMapper;
import cn.tedu.pojo.User;
import cn.tedu.vo.PageResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;




import java.sql.ResultSetMetaData;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author liu
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;


//    @Override
//    public List<User> findAll() {
//        return userMapper.selectList(null);
//    }

    @Override
    public String login(User user) {
        //1.获取明文
        String password = user.getPassword();
        //2.加密处理
        String md5 = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5);
        //3.查询数据库
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
        //4.获取数据库对象
        User userDB = userMapper.selectOne(queryWrapper);
        //5.判断登录是否正确
        if (userDB == null ){
            return null;
        }
        //6.使用UUID动态生成TOKEN，根据当前时间毫秒数+随机数利用hash算法生成
        //几乎可以保证不重复
        String token = UUID.randomUUID().toString()
                .replace("-", "");
        return token;
    }

    /**实现用户分页查询*/
    @Override
    public PageResult getUserList(PageResult pageResult) {
        //1.定义分页对象
        IPage<User> page = new Page<>(pageResult.getPageNum(), pageResult.getPageSize());
        //2.定义条件构造器
        boolean flag = StringUtils.hasLength(pageResult.getQuery());//用户查询框前端
        boolean flagPhone = StringUtils.hasLength(pageResult.getQueryPhone());//手机号查询框
        boolean flagEmail = StringUtils.hasLength(pageResult.getQueryEmail());//邮箱
        boolean flagStatus = StringUtils.hasLength(pageResult.getQueryStatus());//状态
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(flag,"username",pageResult.getQuery());
        queryWrapper.like(flagPhone,"phone",pageResult.getQueryPhone());
        queryWrapper.like(flagEmail,"email",pageResult.getQueryEmail());
        queryWrapper.like(flagStatus,"status",pageResult.getQueryStatus());
        //3.分页查询
        page = userMapper.selectPage(page, queryWrapper);
        //4.从封装后的分页对象中获取数据
        pageResult.setTotal(page.getTotal())
                .setRows(page.getRecords());
         return pageResult;
    }

    /**用户状态修改*/
    @Override
    @Transactional
    public void updateStatus(User user) {
        userMapper.updateById(user);
    }

    /**用户新增*/
    @Override
    @Transactional
    public void addUser(User user) {
        String password = user.getPassword();
        //加密处理
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(password)
                .setStatus(true)
                .setCreated(new Date())
                .setUpdated(user.getCreated());
        userMapper.insert(user);
    }

    /**根据用户id查询用户信息*/
    @Override
    public User getUserById(Integer id) {
        return userMapper.selectById(id);
    }

    /**更新用户*/
    @Override
    @Transactional
    public User updateUser(User user) {
        String password = user.getPassword();
        //加密处理
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(password);
         userMapper.updateById(user);
         return user;
    }

    /**根据id删除用户信息*/
    @Override
    @Transactional
    public void deleteUserById(Integer id) {
        userMapper.deleteById(id);
    }

}
