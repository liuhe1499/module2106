package cn.tedu.service;

import cn.tedu.mapper.RightsMapper;
import cn.tedu.pojo.Rights;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liu
 */
@Service
public class RightsServiceImpl implements RightsService{

    @Autowired
    private RightsMapper rightsMapper;

    @Override
    public List<Rights> getRightsList() {
        //1.查询一级列表信息
        QueryWrapper<Rights> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", 0);
        List<Rights> oneList = rightsMapper.selectList(queryWrapper);
        //2.遍历一级列表
        for (Rights oneRights : oneList){
            //根据一级查询二级
            queryWrapper.clear();//清除之前的条件
            queryWrapper.eq("parent_id", oneRights.getId());
            List<Rights> twoList = rightsMapper.selectList(queryWrapper);
            //将查询的结果封装到一级对象中
            oneRights.setChildren(twoList);
        }
        return oneList;
    }
}
