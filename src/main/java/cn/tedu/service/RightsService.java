package cn.tedu.service;

import cn.tedu.pojo.Rights;

import java.util.List;

/**
 * @author Administrator
 */
public interface RightsService {

    List<Rights> getRightsList();

}
