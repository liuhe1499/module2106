package cn.tedu.service;


import cn.tedu.pojo.User;
import cn.tedu.vo.PageResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * @author liu
 */
public interface UserService {

    PageResult getUserList(PageResult pageResult);

    void updateStatus(User user);

    void addUser(User user);

    User getUserById(Integer id);

    User updateUser(User user);

    void deleteUserById(Integer id);

    String login(User user);

    //List<User> findAll();
}
