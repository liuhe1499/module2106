package cn.tedu.controller;

import cn.tedu.pojo.Rights;
import cn.tedu.service.RightsService;
import cn.tedu.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liu
 */
@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {

    @Autowired
    private RightsService rightsService;
    /**
     * 显示左侧菜单列表，一二级结构
     * */
    @GetMapping("getRightsList")
    public SysResult getRightsList(){
        List<Rights> rightsList=rightsService.getRightsList();
        return  SysResult.success(rightsList);
    }
}
