package cn.tedu.controller;

import cn.tedu.pojo.User;
import cn.tedu.service.UserService;
import cn.tedu.vo.PageResult;
import cn.tedu.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * @author liu
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

//    @GetMapping("/hello")
//    public List<User> hello(){
//
//        return userService.findAll();
//    }

    /**
     * 业务说明：实现用户登录
     * 思想：根据参数,查询数据库
     *      有值：用户名密码正确
     *      没有值：用户名和密码错误
     *  URL：/user/login
     *  参数：username/password
     *  类型：post
     *  返回值结果：SysResult对象 (token)
     * */
    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        //需求： 要求登录成功之后，返回标识符信息
        String token =  userService.login(user);
        //如果token为null，说明登录失败
        if (token == null || token.length()==0){
            return SysResult.fail();
        }
        //否则 正确返回
        return SysResult.success(token);
    }



    /**实现用户分页查询*/
    @GetMapping("/list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 实现用户状态修改
     * */
    @PutMapping ("/status/{id}/{status}")
    public SysResult updateStatus( User user){
        userService.updateStatus(user);
        return SysResult.success();
    }

    /**实现用户新增*/
    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user){
        userService.addUser(user);
        return SysResult.success();
    }

    /**根据用户Id查询用户信息*/
    @GetMapping("/{id}")
    public SysResult getUserById(@PathVariable Integer id){
        User userById = userService.getUserById(id);
        return SysResult.success(userById);
    }

    /**实现用户更新操作*/
    @PutMapping("updateUser")
    public SysResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return SysResult.success();
    }

    /**根据Id删除用户数据*/
    @DeleteMapping("/{id}")
    public SysResult deleteUserById(@PathVariable Integer id){
        userService.deleteUserById(id);
        return SysResult.success();
    }
}
