package cn.tedu.aop;

import cn.tedu.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

/**
 * 对controller层的异常进行拦截
 * @author liu
 */
@RestControllerAdvice
public class SystemAOP {
    @ExceptionHandler({RuntimeException.class, SQLException.class})
    public SysResult exception(Exception e){
        e.printStackTrace();
        return SysResult.fail();
    }
}
